import logging
import os
import sys
import typing as t
from datetime import datetime

from sqlalchemy import MetaData

sys.path.append("./code/")
from case_class import caseScrape
from upload_class import uploadData


logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)


def update_case_details(n_cases=None, beep=False):

    ud = uploadData(os.curdir, beep)
    engine = ud.makeEngine()

    limit_line = f"limit {n_cases}" if n_cases else ""

    # get a list of N case indexes. they must not have any future appearances calendared, have had their most recent appearance
    # in one of the priority court parts, and not already have an attorney. cases are returned in order of how long it's been
    # since the info was last updated.
    cases = ud.from_db(
        f"""
    with recent_appearances as (
        select 
            case_index, 
            max(appearance_datetime) as latest_appearance
        from appearances
        group by case_index
    ), atty_cases as (
        select 
            case_index, 
            scraped_datetime
        from case_updates 
        where defendant_atty <> 'none'
    ), priority_cases as (
        select 
            a.case_index,   
            cu.scraped_datetime
        from appearances a
        inner join recent_appearances ra
            on a.case_index = ra.case_index
            and a.appearance_datetime = ra.latest_appearance	
        left join case_updates cu
            on a.case_index = cu.case_index
        left join dont_track_cases dt
            on a.case_index = dt.case_index
        where 
            dt.case_index is null 
            and (cu.case_found is null OR cu.case_found != 0)
            and (cu.scraped_datetime is null or date(cu.scraped_datetime) < DATE(CONVERT_TZ(UTC_TIMESTAMP(), 'UTC', 'America/New_York')))
            -- and a.part in ('HMP', 'Nuisance Part', 'Part Z-Nuisance', 'Part Z-Active', 'HMP 1', 'HMP 2')
            -- and nullif(cu.defendant_atty, '') is null
    )
    select distinct case_index
    from priority_cases
    order by scraped_datetime asc
    {limit_line};
    """
    )

    case_scrape = caseScrape(beep)
    scrape_time = datetime.now()

    first = True

    # Make shared db connection
    conn = ud.db_connect()
    cursor = conn.cursor()

    # Get table object
    metadata = MetaData(bind=engine)
    metadata.reflect()
    case_updates = metadata.tables["case_updates"]

    for _, row in cases.iterrows():
        case_index = row["case_index"]
        logging.info(
            f"Checking for new attorney and marshal info for case {case_index}"
        )
        logging.debug(f"Row:\n{row}")

        case_scrape.getCaseData(case_index, first)
        first = False

        # don't update if case not found
        if case_scrape.docket_id == "not found":
            updates = [
                {
                    "case_index": case_index,
                    "defendant_atty": "",
                    "marshal": "",
                    "marshal_date": "",
                    "scraped_datetime": scrape_time,
                    "case_found": 0,
                }
            ]
        else: 
            updates = [
                {
                    "case_index": case_index,
                    "defendant_atty": case_scrape.attorney_name,
                    "marshal": case_scrape.marshals[ind],
                    "marshal_date": case_scrape.marshal_dates[ind],
                    "scraped_datetime": scrape_time,
                    "case_found": 1,
                }
                for ind in range(len(case_scrape.marshals))
            ]

        if updates:
            logging.info(f"Found new info, updating the DB")

            try:
                delete_stmt = case_updates.delete().where(
                    case_updates.c.case_index == case_index
                )
                delete_result = delete_stmt.execute()

                for update in updates:
                    insert_stmt = case_updates.insert().values(**update)
                    insert_result = insert_stmt.execute()

            except:
                conn.rollback()
                logging.error(f"Failed to upload case updates to DB")
                raise

    cursor.close()
    conn.close()

    case_scrape.quit()


def main(argv):
    n = None if len(argv) == 1 else argv[1]
    beep = False if len(argv) < 3 else bool(argv[2])
    update_case_details(n_cases=n, beep=beep)


if __name__ == "__main__":
    sys.exit(main(sys.argv))
