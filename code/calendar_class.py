from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from selenium.webdriver.support.ui import Select
from case_extract import extractEntry, case
import time
import os
import re
import dateparser
from datetime import datetime as dt
from config import Config
import pandas as pd

import sys
sys.path.append('./code/')
from case_class import case

class courtCalendar(Config):

    def __init__(self, court, dir_, start_date, end_date=None, beep=False):
        """
        - initiates Config (external variables)
        """
        super().__init__()
        self.court = court
        self.start = start_date
        self.end = end_date
        self.dir_ = dir_
        self.beep = beep
        #TODO add env variable
        #TODO get inpuits from flask

    def getData(self):
        """
        Starts the webdriver, defines wait, runs first captcha, starts scraping calendar
        """
        self.driver = webdriver.Firefox()
        self.wait = WebDriverWait(self.driver, 30)

        self.driver.get('https://iapps.courts.state.ny.us/webcivilLocal')
        self.wait.until(EC.presence_of_element_located((By.NAME, "captcha_form")))
        self.check_captcha()

        self.wait.until(EC.visibility_of_element_located((By.LINK_TEXT, "Court Calendars")))
        self.driver.find_element_by_link_text('Court Calendars').click()

        self.getCalendar()

    def check_captcha(self):
        """
        Holds until captcha is filled out.
        """
        # print('checking for captcha...')
        done = False
        captcha = False
        alert = False

        while not done:
            try:
                if self.driver.find_element_by_name("captcha_form") is not None:
                    captcha = True
                    if not alert:
                        print('captcha needed!')
                        if self.beep:
                            print('\a')
                        alert = True
                    time.sleep(5)
                    continue
            except NoSuchElementException:
                if captcha:
                    print('captcha done!')
                done = True

    def getCalendar(self):
        """
        Starts the calendar scraping process. (Actual scraping by case done by downloadCalendarData)

        - inputs court and date info
        - selects all judges
        - downloads calender html -- this is what downloadCalendarData works from (not the live web page)
        """
        self.chooseCourt()
        self.chooseDate()
        self.driver.find_element_by_name('btnFindCalendar').click()

        # coding around judge select for queens data
        print(self.court)
        # parti sometimes has multiple judges sometimes not. should make this more flexible
        if self.court not in ['qcap1']:
            print('waiting for next screen...')
            self.wait.until(EC.visibility_of_element_located((By.NAME, "btnSelectAll")))

            self.driver.find_element_by_name("btnSelectAll").click()
            self.driver.find_element_by_name("btnApply").click()

        time.sleep(5)

        self.source = self.driver.page_source

        if 'No Future Appearances Found' in self.source:
            print('no future appearances found')
            self.driver.quit()
            return

        with open(self.dir_ + 'calendar.html', 'w') as f:
            f.write(self.driver.page_source)


    def chooseCourt(self, court=None):
        """
        Chooses court for calendar.

        Currently works for both brooklyn HMP parts and queens.

        Add more courts or parts as needed!
        """
        print('choosing court')
        if court is None:
            court = self.court

        if type(court) == tuple:
            court_value = str(court[0])
            court_part = str(court[1])
        
        else:
            if court.lower() == 'qcap1':
                court_value = '22'
                court_part = '9457'

            elif court.lower() == 'hmp':
                court_value = '21'
                court_part = '9796'

            elif court.lower() == 'nuisance':
                court_value = '21'
                court_part = '8954'

            elif court.lower() == 'parti':
                court_value = '21'
                court_part = '10778'

            elif court.lower() == 'parti2':
                court_value = '21'
                court_part = '11399'

            elif court.lower() == 'roch':
                court_value = '824'
                court_part = '7173'

        court_select = Select(self.driver.find_element_by_id('cboCourt'))
        court_select.select_by_value(court_value)

        part_select = Select(self.driver.find_element_by_id('cboCourtPart'))
        part_select.select_by_value(court_part)

    def chooseDate(self):
        """
        Assumes dates in YYYY-MM-DD format.
        """
        print('choosing date')
        for date in ['start', 'end']:

            if date == 'start':
                date_str = 'txt{}calFromDate'

            else:
                date_str = 'txt{}calToDate'

            date = "self.{}".format(date)

            if date is not None:

                date_ = dateparser.parse(eval(date))

                for d_ in ['month', 'day', 'year']:
                    print(eval("date_.{}".format(d_)))
                    element = self.driver.find_element_by_name(date_str.format(d_.title().replace('y', 'te')))
                    element.clear()
                    element.send_keys(eval('date_.{}'.format(d_)))

            else:
                continue

    def downloadCalendarData(self, file_path='./'):
        """
        Crawls and scrapes case data from calendar html (downloaded in getCalendar).

        - collects Case Info in 'cases'
        - collects Case Classes (automatically constructs url for Case Detail, Appearances and Motions) in 'case_classes'
        - collects bad data in 'misses' (not currently used for anything)
        - for each case class, downloads html for Case Detail, Appearances and Motions
        """
        print('parsing...')
        start = 0
        html = open(self.dir_ + 'calendar.html', 'r').read()
        soup = BeautifulSoup(html, 'lxml')

        self.cases = []
        self.case_classes = []
        self.misses = []

        # old method
        for n, t in enumerate(soup.find_all('table')[start:]):
            try:
                self.check_captcha()
                case_entry = extractEntry(t)
                case_index = re.sub(r"[-/]", r"_", case_entry['index_number'])
                case_class = case(t.a['onclick'])
                case_class.dicto_['case_index'] = case_index
                case_class.dicto_['part'] = self.court
                self.cases.append(case_entry)
                self.case_classes.append(case_class)

                time.sleep(2)
                self.driver.get(case_class.case_detail_url)
                with open(self.dir_ + 'case_detail_{}.html'.format(case_index), 'w+') as f:
                    f.write(self.driver.page_source)
                
                self.check_captcha()
                time.sleep(2)
                self.driver.get(case_class.appearances_url) 
                with open(self.dir_ + 'appearances_{}.html'.format(case_index), 'w+') as f:
                    f.write(self.driver.page_source)
                    
                self.check_captcha()
                time.sleep(2)
                self.driver.get(case_class.motions_url) 
                with open(self.dir_ + 'motions_{}.html'.format(case_index), 'w+') as f:
                    f.write(self.driver.page_source)

            except Exception as e:
                print(e.args)
                self.misses.append([start+n, e])

        # save all case info
        self.case_info_df = pd.DataFrame(self.cases)

        self.driver.quit()
    
