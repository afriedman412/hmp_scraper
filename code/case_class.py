import os
from bs4 import BeautifulSoup
import pandas as pd
import re
import shutil
import time
from functools import partial

from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from selenium.webdriver.common.keys import Keys

class case:
    def __init__(self, case_data):
        
        reg = r"(?<=(openLCCaseDetailsWindow\()).*?(?=\))"
        cols = ['county', 'index', 'motion_count', 'document_count', 'date']
        data = [l.replace("\'", "") for l in re.search(reg, case_data)[0].split("', '")[2:]]
        dicto_ = dict(zip(cols, data))
        dicto_['date'] = dicto_['date'].replace(' ', '%20')
        
        self.dicto_ = dicto_
        
        # make case_detail url
        url_dict = {
            'parm': 'CaseInfo',
            'indexNumber': self.dicto_['index'],
            'courtId': self.dicto_['county'],
            'motionCount': self.dicto_['motion_count'],
            'documentCount': self.dicto_['document_count'],
            'appearanceDate': self.dicto_['date']
        }

        url = 'https://iapps.courts.state.ny.us/webcivilLocal/LCCaseInfo?'
        url +='&'.join(['='.join([k,v]) for k,v in url_dict.items()])
        self.case_detail_url = url
        
        # make appearances_url
        url_dict = {
            'parm': '',
            'index': self.dicto_['index'],
            'county': self.dicto_['county'],
            'civilCaseId': 'undefined'
        }

        url = 'https://iapps.courts.state.ny.us/webcivilLocal/LCAppearanceDetail?'
        url +='&'.join(['='.join([k,v]) for k,v in url_dict.items()])
        self.appearances_url = url

       # make motions url
        url_dict = {
            'parm': 'Motion',
            'indexNumber': self.dicto_['index'],
            'courtId': self.dicto_['county']
        }

        url = 'https://iapps.courts.state.ny.us/webcivilLocal/LCCaseInfo?'
        url +='&'.join(['='.join([k,v]) for k,v in url_dict.items()])
        self.motions_url = url


class caseScrape():
    def __init__(self, beep=False):
        self.init_case()
        self.beep = beep

        self.download_dir_ = os.path.join(os.path.abspath(os.path.expanduser('.')), 'case_files')
        self.temp_dir_ = os.path.join(os.path.abspath(os.path.expanduser('.')), 'calendar_html_temp')
        try:
            os.mkdir(self.download_dir_)
            os.mkdir(self.temp_dir_)
        except FileExistsError:
            pass
        
        mime_types = "application/pdf,application/vnd.adobe.xfdf,application/vnd.fdf,application/vnd.adobe.xdp+xml"

        fp = webdriver.FirefoxProfile()
        fp.set_preference("browser.download.folderList", 2)
        fp.set_preference("browser.download.manager.showWhenStarting", False)
        fp.set_preference("browser.download.dir", self.download_dir_)
        fp.set_preference("browser.helperApps.neverAsk.saveToDisk", mime_types)
        fp.set_preference("plugin.disable_full_page_plugin_for_types", mime_types)
        fp.set_preference("pdfjs.disabled", True)

        print("Running Firefox Webdriver to scrape court data ...")
        self.driver = webdriver.Firefox(firefox_profile=fp)
        self.wait = WebDriverWait(self.driver, 30)

    def init_case(self):
        self.address = 'ADDRESS ERROR'
        self.tenant_name = 'TENANT ERROR'
        self.attorney_name = 'none'
        self.marshal = 'none'
        self.marshal_date = 'none'

    def quit(self):
        self.driver.quit()

    def captchaCheck(self, func=None):
        """
        Holds until captcha is filled out.
        """
        # print('checking for captcha...')
        done = False
        captcha = False
        alert = False

        while not done:
            try:
                if self.driver.find_element_by_name("captcha_form") is not None:
                    captcha = True
                    if not alert:
                        print('captcha needed!')
                        if self.beep:
                            print('\a')
                        alert = True
                    time.sleep(5)
                    continue
            except NoSuchElementException:
                if captcha:
                    print('captcha done!')
                done = True
    
    # def captchaCheck(self, func=None):
    #     """
    #     Holds until captcha is filled out.
    #     """
    #     # print('checking for captcha...')
    #     done = False
    #     captcha = False
    #     alert = False

    #     while not done:
    #         try:
    #             if self.driver.find_element_by_name("captcha_form") is not None:
    #                 captcha = True
    #                 if not alert:
    #                     print('captcha needed!')
    #                     alert = True
    #                 time.sleep(5)
    #                 continue
    #         except NoSuchElementException:
    #             if captcha:
    #                 time.sleep(5)
    #                 # print('captcha done!')
    #             # done = True

    # def captchaCheck(self, func=None):
    #     waiting = True
    #     while waiting:
    #         try:
    #             if self.driver.find_element_by_class_name('g-recaptcha'):
    #                 waiting = False
    #             else:
    #                 time.sleep(3)
    #         except NoSuchElementException:
    #             print('no such')
    #             time.sleep(3)
    #     # if self.driver.find_elements_by_class_name('jCaptchaBox'):
    #     # if self.driver.find_element_by_class_name('g-recaptcha'):
    #     print("...waiting for captcha...")
    #     try:
    #         self.wait.until(
    #             EC.presence_of_element_located((By.CSS_SELECTOR, 
    #             ".MsgBox_SearchCriteria, .CaseSummary, [id=txtCaseIdentifierNumber]")
    #             ))
    #         return
    #     except TimeoutException:
    #         if func is not None:
    #             i = input("timeout. restart? (Y/n)")
    #             if i.lower() == 'y' or len(i) == 0:
    #                 self.driver.quit()
    #                 self.launchBrowser()
    #                 func()
    #                 return
    #             else:
    #                 return
        # else:
        #     return

    def writeHTML(self, case_id_):
        self.driver.get(self.docket_link)
        self.captchaCheck(
            func=partial(self.writeHTML, case_id_=case_id_)
            )
        with open(self.temp_dir_+ '/case_detail_{}.html'.format(case_id_), 'w') as f:
            f.write(self.driver.page_source)

    def scrapeHTML(self):
        soup = BeautifulSoup(self.driver.page_source, 'lxml')
        address = soup.find('div',
            attrs={'class':"DataEntry_InnerBox"}).text.split('Property Address')[1].split('Description :')[0].split('Petitioners')[0]
        self.address = ' '.join(address.split())

        respondents_table = soup.find('table', attrs={'summary':'Respondents in this case'})
        
        self.tenant_name = ",".join(
            set(
                [
                    x.find_all(text=True)[1].strip()
                    for x in respondents_table.tbody.find_all("tr")
                ]
            )
        )
        self.attorney_name = max([x.find_all(text=True)[5].strip() for x in respondents_table.tbody.find_all('tr')])

        agents_table = soup.find('table', attrs={'summary':'This table contains all the authorized agents in this case'})
        if agents_table:
            self.marshals = [x.find_all(text=True)[3].strip() for x in agents_table.tbody.find_all('tr')]
            self.marshal_dates = [x.find_all(text=True)[5].strip() for x in agents_table.tbody.find_all('tr')]
            print(f"Found {len(self.marshals)}, authorized agents (maybe marshals)")
        else:
            self.marshals = ['']
            self.marshal_dates = ['']

    def getCaseData(self, case_id, first=False):
        self.init_case()

        case_id_ = re.sub(r"[-\/]", "_", case_id)
        driver = self.driver
        driver.get('https://iapps.courts.state.ny.us/nyscef/CaseSearch')

        if first:
            self.wait.until(EC.presence_of_element_located((By.NAME, "captcha_form")))

        self.captchaCheck(
            func=partial(self.getCaseData, case_id=case_id)
            )

        # enter case ID
        print(case_id_)
        self.wait.until(EC.presence_of_element_located((By.ID, "txtCaseIdentifierNumber")))
        inputElement = driver.find_element_by_id("txtCaseIdentifierNumber")
        inputElement.send_keys(case_id)
        driver.find_element_by_name('btnSubmit').click()

        # look for link to case info
        try:
            c = driver.find_element_by_link_text(case_id)
            link = c.get_attribute('href')
            self.docket_id = link.split('?docketId=')[1].split('&display=')[0]
            self.docket_link = 'https://iapps.courts.state.ny.us/nyscef/CaseDetails?docketId={}'.format(self.docket_id)
            c.click()
            self.captchaCheck(
                func=partial(self.getCaseData, case_id=case_id)
            )

        # exit if not found
        except NoSuchElementException:
            self.docket_id = 'not found'
            print('*** case not found!')
            return False

        # setting up download directory
        case_dir_ = os.path.join(self.download_dir_, case_id_)
        if os.path.exists(case_dir_) is False:
            print('making new case dir')
            os.mkdir(case_dir_)

        print('downloading pages...')
        html = self.writeHTML(case_id_)
        
        print('extracting data...')
        try:
            self.scrapeHTML()
        except Exception as e:
            print(e.args)
        
        # move files out of temp directory
        for file in os.listdir(self.temp_dir_):
            try:
                shutil.move(os.path.join(self.temp_dir_, file), case_dir_)
            except:
                pass

        print('{} done!!'.format(case_id_))
        



