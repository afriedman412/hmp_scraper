from bs4 import BeautifulSoup
import requests
import re
import datetime
import dateparser
import pandas as pd

filters = {
    'OutcomeType': ['Reserved Decision', 'Granted'], 'AppearanceDate': datetime.datetime.today()
    }


def getCaseData(link):
    reg = r"(?<=(openLCCaseDetailsWindow\()).*?(?=\))"
    cols = ['county', 'index', 'motion_count', 'document_count', 'date']
    data = [l.replace("\'", "") for l in re.search(reg, link)[0].split("', '")[2:]]
    dicto_ = dict(zip(cols, data))
    dicto_['date'] = dicto_['date'].replace(' ', '%20')
    return dicto_

def extractEntry(t):
    case_name = t.find('dt').a.text

    date_ = dateparser.parse(t.a['onclick'].split("'', '")[-1])
    date_ = date_.strftime("%m/%d/%Y")

    dicto = {
        'index_number': t.find('dt').text.strip().split(' -\n')[0],
        'case_name': case_name,
        'plaintiff': case_name.split('vs.')[0].strip(),
        'defendant': case_name.split('vs.')[1].strip(),
        'case_date': date_
    }

    for k in ['time', 'on_for', 'plaintiff_atty', 'defendant_atty', 'relief_sought']:
        dicto[k] = 'none'

    data = [d.text for d in t.find_all('dd')]
    for d in data:
        for i in [
            ['time', 'Time: '],
            ['plaintiff_atty', 'Plaintiff Attorney: '],
            ['defendant_atty', 'Defendant Attorney: ']
        ]:
            if i[1] in d:
                dicto[i[0]] = d.replace(i[1], '')

        if 'Relief Sought' in d:
            dicto['on_for'] = d.split('\n')[1]
            dicto['relief_sought'] = d.split('Relief Sought:')[-1].strip()

    return dicto

class case:
    def __init__(self, case_data):
        self.dicto_ = case_data
        self.dicto_['document_count'] = self.dicto_['spacer']

        # make case_detail url
        url_dict = {
            'parm': 'CaseInfo',
            'indexNumber': self.dicto_['index'],
            'courtId': self.dicto_['county'],
            'motionCount': self.dicto_['motion_count'],
            'documentCount': self.dicto_['document_count'],
            'appearanceDate': self.dicto_['date']
        }

        url = 'https://iapps.courts.state.ny.us/webcivilLocal/LCCaseInfo?'
        url +='&'.join(['='.join([k,v]) for k,v in url_dict.items()])
        self.case_detail_url = url

        # make appearances_url
        url_dict = {
            'parm': '',
            'index': self.dicto_['index'],
            'county': self.dicto_['county'],
            'civilCaseId': 'undefined'
        }

        url = 'https://iapps.courts.state.ny.us/webcivilLocal/LCAppearanceDetail?'
        url +='&'.join(['='.join([k,v]) for k,v in url_dict.items()])
        self.appearances_url = url

       # make motions url
        url_dict = {
            'parm': 'Motion',
            'indexNumber': self.dicto_['index'],
            'courtId': self.dicto_['county']
        }

        url = 'https://iapps.courts.state.ny.us/webcivilLocal/LCCaseInfo?'
        url +='&'.join(['='.join([k,v]) for k,v in url_dict.items()])
        self.motions_url = url

def parseCalendar(html, start=6):
    cases = []
    for n, t in enumerate(soup.find_all('table')[start:]):
        try:
            cases.append(extractEntry(t))
        except Exception as e:
            pass

    return cases


def parseMotions(html):
    all_tables = pd.read_html(html)
    case_labels = all_tables[2].T.values[0]
    case_info = all_tables[2].T.values[2]
    case_dict = dict(zip(case_labels, case_info))

    motions_dict = all_tables[3].fillna('none').to_dict('records')
    cols = ['MotionNumber', 'DateFiled', 'FiledBy', 'Relief Sought',
           'Nature of Decision/Judge', 'Decision Date', 'OrderSigned Date']

    motions = []
    for m in motions_dict:
        values = m.values()
        dicto_ = dict(zip(cols, values))
        judge_split = dicto_['Nature of Decision/Judge'].split('Before Judge:')
        dicto_['Judge'] = ''
        dicto_['Nature of Decision'] = judge_split[0].strip()
        if len(judge_split) > 1:
            dicto_['Judge'] = judge_split[1].strip()
        del dicto_['Nature of Decision/Judge']
        dicto_.update(case_dict)
        motions.append(dicto_)

    return motions


def parseJudgePart(jp):
    judgepart = jp.split('\xa0')
    judge = judgepart[0]
    if len(judgepart) > 1:
        part = judgepart[1]
    else:
        part = ''
    return judge, part


def parseAppearances(html):
    all_tables = pd.read_html(html)
    case_info = all_tables[2]
    # case_info = pd.read_html(html)[2].to_dict('records')

    # case_cols = [c_['_0'].replace(':','').strip() for c_ in case_info]
    # case_data = [c_['_1'].strip() for c_ in case_info]
    # case_dict = dict(zip(case_cols, case_data))
    case_dict = {v[0]:v[1] for v in case_info.T.to_dict().values()}

    appearances_df = pd.read_html(html)[3]
#    appearances_unfiltered = appearances_df.to_dict('records')

    if 'JudgePart' not in appearances_df.columns:
        n = 0
        try:
            while 'JudgePart' not in appearances_df.columns:
                print(n)
                appearances_df = pd.read_html(html)[n]
                print(appearances_df.columns)
                n += 1
        except IndexError:
            appearances_df['JudgePart'] = ''
            pass

    appearances_df['JudgePart'] = appearances_df['JudgePart'].map(lambda j: j.replace('Housing Motion Part', '').strip())
    appearances_df['Judge'] = appearances_df['JudgePart'].map(lambda j: parseJudgePart(j)[0])
    appearances_df['Part'] = appearances_df['JudgePart'].map(lambda j: parseJudgePart(j)[1])
    appearances = appearances_df.drop(columns='JudgePart').to_dict('records')
    # appearances_final = filterAppearances(appearances)
    appearances_final = appearances

    for a in appearances_final:
        a.update(case_dict)
    return appearances_final

def filterAppearances(appearances):
    output = []
    filtered_appearances = filter(is_past, appearances)
    #if you want all appearances, just blank out the filters variable
    if len(filters) >= 1:
        return list(map(checkAppearance, filtered_appearances))
    else:
        return filtered_appearances

def is_past(a):
    # make sure nothing gets in from before oct. 1
    for key, value in filters.items():
        if isinstance(value, datetime.date):
            appearance_date = datetime.datetime.strptime(a[key], '%m/%d/%Y')
            if appearance_date.date() < datetime.date(2020, 10, 1):
                return False
            else:
                return True


def checkAppearance(a):
    for key, value in filters.items():
        # this was some real unnecessary premature optimization where i didn't use actual attribute names
        # probably could just have keys/values in an env file
        if isinstance(value, datetime.date):
            appearance_date = datetime.datetime.strptime(a[key], '%m/%d/%Y')
            if appearance_date.date() > value.date():
                a['future_date_watch_list'] = True
                return a
            continue
        print('value: ', value)
        print('a[key]', a[key])
        if isinstance(a[key], str) and any( i in a[key] for i in value):
            a['possible_eviction'] = True
    return a



def parseCaseDetail(html):
    soup = BeautifulSoup(html, 'lxml')
    t = soup.find_all('table')[3]

    cols = [pd.read_html(html)[2].loc[n,0].replace(':', '') for n in range(1,pd.read_html(html)[2].shape[0])]
    try:
        vals = [pd.read_html(html)[2].fillna('none').loc[n,1] for n in range(1,pd.read_html(html)[2].shape[0])]

    except KeyError:
        return []

    detail_dict = dict(zip(cols, vals))



    atty_track = False
    atty_data = []
    data = []
    for tr in t.find_all('tr'):
        if 'Attorney/Firm(s)' in tr.text:
            if atty_track:
                atty_data.append(data)

            atty_track = True
            data = []
        if atty_track:
            data.append(tr.text.strip())

    atty_data.append(data)

    all_atty = []
    for a in atty_data:
        atty = {
            'info': [],
            'atty_type': ''
        }
        atty.update(detail_dict)

        for d in a:
            if 'Attorney/Firm(s)' in d:
                atty['party_type'] = d.split(' - ')[0].replace('Attorney/Firm(s) For ', '')
                atty['party_name'] = d.split(' - ')[1].replace(':', '')

            elif 'Attorney Type:' in d:
                atty['atty_type'] = d.split('Attorney Type:')[-1].strip()
                atty['info'].append(d.split('Attorney Type:')[0].strip().replace('\\n', ''))

            else:
                t = d.strip().replace('\n', ' ')
                if len(t) > 0:
                    atty['info'].append(t)


        atty['info'] = ' / '.join(atty['info'])
        all_atty.append(atty)
    return all_atty
