from case_extract import (
    extractEntry,
    case,
    parseMotions,
    parseAppearances,
    parseCaseDetail,
)
from case_class import caseScrape
from bs4 import BeautifulSoup
from sqlalchemy import create_engine
import pymysql
import time
import os
import re
import dateparser
from datetime import datetime as dt
from config import Config
import pandas as pd
from helpers import sqlFormatter
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


class uploadData(Config):
    def __init__(self, dir_, beep=False):
        super().__init__()

        self.dir_ = dir_
        self.beep = beep

    def db_connect(self):
        return pymysql.connect(
            host=self.db_host,
            user=self.db_user,
            password=self.db_password,
            database=self.db_database,
        )

    def makeEngine(self, echo=True):
        mydbstr = """mysql+pymysql://{}:{}@{}:{}/{}"""
        return create_engine(
            mydbstr.format(
                self.db_user,
                self.db_password,
                self.db_host,
                str(self.db_port),
                self.db_database,
            ),
            echo=echo,
        )

    def from_db(self, query, parse_dates=None):
        conn = self.db_connect()
        df = pd.read_sql(query, conn, parse_dates=parse_dates)
        conn.commit()
        conn.close()
        return df

    def to_db(self, df, table):
        print("executing sql...")
        engine = self.makeEngine()
        df.to_sql(table, con=engine, if_exists="append", index=False)
        print("closing engine")
        engine.dispose()
        print("done")

    def q(self, q):
        conn = self.db_connect()
        cursor = conn.cursor()
        cursor.execute(q)
        conn.commit()
        cursor.close()
        conn.close()

    def getAddresses(self, indexes_to_scrape):
        scraper = caseScrape(self.beep)

        # try to do that captcha straight away
        scraper.driver.get("https://iapps.courts.state.ny.us/nyscef/CaseSearch")
        scraper.wait.until(EC.presence_of_element_located((By.NAME, "captcha_form")))
        scraper.captchaCheck()

        addresses = []
        for n, c in enumerate(indexes_to_scrape):
            print(
                "getting info for case {} of {}".format(
                    str(n), str(len(indexes_to_scrape))
                )
            )

            scraper.getCaseData(c)

            dicto_ = {
                "Case Index": c,
                "Tenant Address": scraper.address,
                "Tenant Name": scraper.tenant_name,
                "Docket ID": scraper.docket_id,
            }

            print(dicto_)
            addresses.append(dicto_)

        scraper.quit()
        return pd.DataFrame(addresses)

    def checkForNewAddresses(self):
        """
        Finds case indexes that aren't already in the tenants table.
        """
        tenants = self.from_db("select distinct case_index from tenants")
        new_tenants = self.appearance_df[
            ~self.appearance_df["case_index"].isin(tenants["case_index"])
        ]
        return new_tenants

    def parseHtmltoDataFrames(self, data_type, func):
        """
        Converts downloaded html for Appearances or Motions into dataframe.

        (Also downloads all data as .csv)
        """
        data_entries = []
        print(len(os.listdir(self.dir_)))
        for f in os.listdir(self.dir_):
            if (data_type in f) and ("html" in f):
                print(f)
                print("**")
                print(len(data_entries))
                html = open(self.dir_ + f, "r").read()
                if (
                    "You are using an expired link that points to an individual case. Use one of the search links in the left menu to find your case"
                    in html
                ):
                    print("expired link, skipping")
                    continue
                else:
                    data = func(html)
                    data_entries += data

        df = pd.DataFrame(data_entries)
        print(df.shape)
        df.to_csv(self.dir_ + "{}_dump.csv".format(data_type), index=False)

        return df

    def getAtty(self, i):
        """
        Adds outside data to Appearance data
        - currently only used for defendant attorney
        """
        try:
            lawyers = self.case_info_df.set_index("index_number").loc[
                i, "defendant_atty"
            ]
            if type(lawyers) is not str:
                if lawyers.nunique() > 1:
                    return "verify"
                else:
                    return lawyers.unique()[0]
            else:
                return lawyers
        except KeyError:
            return "no data"

    def appearanceCleanerRemover(self):
        """
        After adding new appearances...
        - download all appearance datae
        - standardizes missing attorney data
        - fix any missing attorneys
        - drop duplicates
        - converts missing outcomes to strings
        - removes appearances with updated outcomes
        """

        def attorneyCleaner(df):
            """
            Input -- df of appearances for a single case index

            fills in any "no data" if there is a legit attorney
            """
            if df["defendant_atty"].nunique() == 2:
                atty = [d for d in df["defendant_atty"].unique() if d != "no data"][0]
                df["defendant_atty"] = atty

            return df

        conn = self.db_connect()
        all_appearances = pd.read_sql("select * from appearances", conn)
        all_appearances["defendant_atty"] = all_appearances["defendant_atty"].map(
            lambda a: "no data" if a == "none" else a
        )
        all_appearances = all_appearances.groupby("case_index").apply(attorneyCleaner)
        all_appearances = all_appearances.drop_duplicates()
        all_appearances["outcome"] = all_appearances["outcome"].fillna("NO_OUTCOME")
        all_appearances = all_appearances.groupby(
            ["case_index", "part", "appearance_datetime"]
        ).apply(lambda r: r[r["outcome"] != "NO_OUTCOME"] if len(r) > 1 else r)

        all_appearances = all_appearances.reset_index(drop=True)

        # get original appearances size
        self.all_appearances_prime = len(all_appearances)

        print("deleting old appearances")
        self.q("delete from appearances")

        print("uploading new appearances")
        self.to_db(all_appearances, "appearances")

        # get new appearances size
        self.all_appearances_updated = len(all_appearances)

    def updateRunTracker(self, start_date, end_date, court):
        to_add = {
            "date_completed": dt.now(),
            "court_part": str(court),
            "start_date": start_date,
            "end_date": end_date,
            "appearances_start": self.all_appearances_prime,
            "appearances_updated": self.all_appearances_updated,
            "new_tenants": self.new_tenants_size,
        }

        self.to_db(pd.DataFrame(to_add, index=[0]), "run_tracker")

    def parseProcessAndUpload(self):
        """
        Highest order function for processing the downloaded court data.
        - constructs case_info df if only parsing (otherwise it would happen during scraping the calendar)
        """

        print("case info")
        # case_info_df gets made during the initial scrape of the calendar, so need to do this if ur only parsing
        if "case_info" not in self.__dict__:
            cases = []
            html = open(self.dir_ + "calendar.html", "r").read()
            soup = BeautifulSoup(html, "lxml")
            start = 6
            for n, t in enumerate(soup.find_all("table")[start:]):
                try:
                    cases.append(extractEntry(t))
                except Exception as e:
                    pass

            self.case_info_df = pd.DataFrame(cases)

        self.appearance_df = self.parseHtmltoDataFrames("appearance", parseAppearances)

        print("adding defendant atty info")
        self.appearance_df["Defendant Atty"] = self.appearance_df["Index Number:"].map(
            self.getAtty
        )

        print("reformatting for sql")
        s = sqlFormatter()
        self.appearance_df = s.appearances(self.appearance_df)
        self.case_info_df = s.case_info(self.case_info_df)

        print("uploading appearances to db")
        self.to_db(self.appearance_df, "appearances")

        print("cleaning appearances and re-upping to db")
        self.appearanceCleanerRemover()

        print("uploading case_info to db")
        self.to_db(self.case_info_df, "case_info")

        print("checking for new tenants")
        indexes_to_scrape = self.checkForNewAddresses()

        # get size of new tenants
        self.new_tenants_size = len(indexes_to_scrape)

        if len(indexes_to_scrape) > 0:
            print("getting new tenant addresses")
            self.new_tenants_df = self.getAddresses(
                indexes_to_scrape["case_index"].unique()
            )
            print("uploading new tenant addresses to db")
            self.new_tenants_df = s.tenants(self.new_tenants_df)
            self.to_db(self.new_tenants_df, "tenants")

        print("done!")

    def make_csv_folders(self, table: str) -> None:
        path = os.path.join("csvs_temp", table)
        folder_exists = os.path.exists(path)

        if not folder_exists:
            os.makedirs(path)

    def stash_csv(self, df: pd.DataFrame, filename: str) -> None:
        df.to_csv(filename, index=False)

    def push_csv_to_db(self, table: str, filename: str) -> None:
        df = pd.read_csv(filename)
        self.to_db(table)

    def stash_and_push(self, table: str, df: pd.DataFrame) -> None:
        self.make_csv_folders(table)
        current_time = dt.now().strftime("%Y-%m-%d_%H:%M:%S")
        filename = os.path.join("csvs_temp", table, f"{current_time}.csv")
        self.stash_csv(df, filename)
        self.push_csv_to_db(table, filename)
