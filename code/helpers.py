import pandas as pd
from datetime import datetime as dt

class sqlFormatter:
    """
    Columns were halfassedly named for google sheets, this is cleaning them up when migrating/importing to database.
    """
    def __init__(self):

        self.tenants_map = [
            ['Case Index', 'case_index'],
            ['Tenant Address', 'tenant_address'],
            ['Tenant Name', 'tenant_name'],
            ['Docket ID', 'docket_id'],
        ]

        self.appearances_map = [
            ['Index Number:', 'case_index'],
            ['AppearanceDate', 'appearance_date'],
            ['Time', 'appearance_time'],
            ['Purpose', 'purpose'],
            ['OutcomeType', 'outcome'],
            ['Part', 'part'],
            ['MotionSeq', 'motion_seq'],
            ['Judge', 'judge'],
            ['Case Name:', 'case_name'],
            ['Case Type:', 'case_type'],
            ['Court:', 'court'],
            ['Defendant Atty', 'defendant_atty'],
        ]

    def combineDateTimeCols(self, df, date_col='appearance_date', time_col='appearance_time'):
        df[date_col] = df[date_col].dt.strftime("%Y-%m-%d")
        return pd.to_datetime(df.apply(lambda r: ' '.join([r[date_col], r[time_col]]), 1))

    def appearances(self, df):
        df_sql = df.rename(
            columns={
                    c[0]:c[1] for c in self.appearances_map if c[0] in df.columns
                }
            )

        df_sql['appearance_date'] = pd.to_datetime(df_sql['appearance_date'])
        df_sql['appearance_datetime'] = self.combineDateTimeCols(df_sql)

        appearance_sql_cols = ['case_index', 'appearance_datetime', 'purpose',
            'outcome', 'part', 'motion_seq', 'judge', 'case_name', 'case_type',
            'court', 'defendant_atty']

        df_sql = df_sql[appearance_sql_cols]
        return df_sql

    def case_info(self, df):
        df_sql = df.rename(columns={
            'index_number':'case_index'
        })
        df_sql['case_date'] = pd.to_datetime(df_sql['case_date'].fillna(dt(1900,1,1)))
        df_sql['case_datetime'] = self.combineDateTimeCols(df_sql, 'case_date', 'time')
        return df_sql

    def tenants(self, df):
        df_sql = df.rename(
            columns={
                    c[0]:c[1] for c in self.tenants_map if c[0] in df.columns
                }
            )
        df_sql.drop([c for c in df_sql.columns if c not in [c[1] for c in self.tenants_map]], 1, inplace=True)
        return df_sql