import logging
import os
import sys

from sqlalchemy import BigInteger, Column, DateTime, Float, MetaData, String, Table, Text

from config import Config
from upload_class import uploadData


class TableCreator():
    def __init__(self, dir):
        self.dir = dir

    def create_tables(self):
        engine = uploadData(self.dir).makeEngine()
        metadata = MetaData(bind=engine)
        metadata.reflect()

        Table('appearances',
              metadata,
              Column('case_index', String(255)),
              Column('appearance_datetime', DateTime),
              Column('purpose', Text),
              Column('outcome', Text),
              Column('part', Text),
              Column('motion_seq', Float),
              Column('judge', Text),
              Column('case_name', Text),
              Column('case_type', Text),
              Column('court', Text),
              Column('defendant_atty', Text),
              extend_existing=True)

        Table('case_info',
              metadata,
              Column('case_index', String(255)),
              Column('case_name',Text),
              Column('plaintiff', Text),
              Column('defendant', Text),
              Column('case_date', Text),
              Column('time', Text),
              Column('on_for', Text),
              Column('plaintiff_atty', Text),
              Column('defendant_atty', Text),
              Column('relief_sought', Text),
              Column('case_datetime', DateTime),
              extend_existing=True)

        Table('run_tracker',
              metadata,
              Column('date_completed', DateTime),
              Column('court_part', Text),
              Column('start_date', Text),
              Column('end_date', Text),
              Column('appearances_start', BigInteger),
              Column('appearances_updated', BigInteger),
              Column('new_tenants', BigInteger),
              extend_existing=True)

        Table('tenants',
              metadata,
              Column('case_index', String(255)),
              Column('tenant_address', Text),
              Column('tenant_name', Text),
              Column('docket_id', Text),
              Column('status', Text),
              extend_existing=True)

        Table('case_updates',
              metadata,
              Column('case_index', String(255)),
              Column('defendant_atty', Text),
              Column('marshal', Text),
              Column('marshal_date', Text),
              Column('scraped_datetime', DateTime),
              Column('case_found', BigInteger),
              extend_existing=True)

        metadata.create_all(engine)

def main(argv):
    print('Creating database tables')
    table_creator = TableCreator(os.curdir)
    table_creator.create_tables()
    print('Success')


if __name__ == "__main__":
    sys.exit(main(sys.argv))

# alter = engine.execute("ALTER TABLE case_info ADD marshal TEXT")
# alter = engine.execute("ALTER TABLE case_info ADD marshal_date TEXT")