with appearance_info as (
	-- for some reason adding group by messes up the window functions
	select 
		a.case_index,
		GROUP_CONCAT(
			concat(date(a.appearance_datetime), '; ', a.part, '; ', a.outcome) 
			order by a.appearance_datetime desc
			separator '\n'
		) as appearances_info
	from appearances as a
	group by case_index	
), atty_marshal_info_multi as (
	select 
		case_index,
		case 
			when case_found = 0 then ''
			when defendant_atty in ('', 'none') then concat('No as of ', cast(scraped_datetime as date))
			else 'Yes'
		end as has_atty,
		case
			when case_found = 0 then ''
			when marshal in ('', 'none') then concat('No as of ', cast(scraped_datetime as date)) 
			else 'Yes'
		end as marshal_assigned,
		marshal,
		case_found
	from case_updates
), atty_marshal_info as (
	select 
		case_index,
		group_concat(has_atty) as has_atty,
		group_concat(marshal_assigned) as marshal_assigned,
		group_concat(marshal) as marshal,
		case
			when sum(case_found) > 0 then 'found'
			when sum(case_found) = 0 then 'not found'
			else ''
		end as case_found
	from atty_marshal_info_multi
	group by case_index
)
select distinct
	t.case_index, 
	t.tenant_address, 
	t.tenant_name, 
	case when
		t.docket_id in ('not found', 'DOCKET_ID_MISSING') then null
		else concat('https://iapps.courts.state.ny.us/nyscef/DocumentList?docketId=', t.docket_id, '&display=all')
	end as case_info_link,
	ai.appearances_info,
	FIRST_VALUE(date(a.appearance_datetime)) OVER (PARTITION BY case_index ORDER BY a.appearance_datetime desc) AS lateset_appearance,
	FIRST_VALUE(a.part) OVER (PARTITION BY case_index ORDER BY a.appearance_datetime desc) AS latest_part,
	FIRST_VALUE(a.outcome) OVER (PARTITION BY case_index ORDER BY a.appearance_datetime desc) AS lateset_outcome,
	am.has_atty,
	am.marshal_assigned,
	am.marshal,
	am.case_found
FROM tenants as t
inner join appearances as a using(case_index)
inner join appearance_info as ai using(case_index)
left join atty_marshal_info as am using(case_index)
where t.case_index like 'LT%/KI' and tenant_address != 'ADDRESS ERROR';
