import os
import json
from pathlib import Path

class Config:
    def __init__(self):
        self.dir_ = Path(__file__).parents[1]
        if "HEROKU_TEST" not in os.environ:
            config_ = json.load(open(self.dir_ / 'code/config.json', 'rb'))
            for k in config_:
                exec('self.{} = config_["{}"]'.format(k, k))

        else:
            for k in os.environ:
                exec('self.{} = os.environ["{}"]'.format(k, k))

