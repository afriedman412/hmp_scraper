from flask import Flask, request, render_template
import pymysql
import pandas as pd
import datetime as dt
import dateparser
import os
import sys
import tablib

code_path = os.path.join(os.getcwd() + '/code')
print(code_path)

sys.path.append(code_path)
from calendar_class import courtCalendar
from upload_class import uploadData

app = Flask(__name__)


dir_ = os.getcwd() + '/calendar_html_temp/'
try:
    os.mkdir(dir_)
except FileExistsError:
    pass


@app.route("/court_part_key")
def courtPartKey():
    data = tablib.Dataset()
    with open(os.path.join(os.path.dirname(__file__),'court_part_key.csv')) as f:
        data.csv = f.read()
    return data.html

@app.route('/')
def home():
    return render_template('scraper.html')

@app.route('/export')
def exportMenu():
    return render_template('export.html')

@app.route('/export_run')
def exportRun():
    start_date = request.args.get('start_date')
    end_date = request.args.get('end_date')
    file_name = request.args.get('file_name')
    if not file_name:
        file_name = "_".join([start_date, end_date]) + ".csv"
    beep = request.args.get('beep')

    db = uploadData("./", beep)
    q = """
    select * from appearances a, tenants t
    where a.case_index = t.case_index
    and appearance_datetime between "{}" and "{}"
    order by appearance_datetime desc;
    """.format(start_date, end_date)

    df = db.from_db(q)
    df.to_csv(file_name + ".csv")

    return """
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Export Done</title>
    </head>

    <h1>Your export is done!</h1>
    """

@app.route('/scrape', methods=["GET", "POST"])
def startScraper():
    start_date = request.args.get('start_date')
    end_date = request.args.get('end_date')
    if 'court' in request.args:
        court = request.args.get('court')
    else:
        court = (
            request.args.get('court_value'),
            request.args.get('court_part')
            )
    beep = request.args.get('beep')

    now_ = dt.datetime.now()
    if (now_ - dateparser.parse(start_date)).days > 13:
        print('invalid start date ... resetting')
        date_ = (now_ - dt.timedelta(days=14))
        start_date = date_.strftime("%Y-%m-%d")
        print(start_date)

    court_data = courtCalendar(court, dir_, start_date, end_date, beep)

    if request.args.get('parse_only') != 'on':
        court_data.getData()
        court_data.downloadCalendarData()
    else:
        print('parsing only!')

    uploader = uploadData(dir_, beep)
    uploader.parseProcessAndUpload()
    uploader.updateRunTracker(start_date, end_date, court)
    return render_template('scraping.html')

if __name__ == '__main__':
    app.run(threaded=True, port=5000)