# Housing Motion Part Scraper
A Python script that makes looking up data on Housing Motion Part cases easy and fun.

Court data, like so many public documents, is technically "public information", but actually getting to it involves navigating lots of trash government websites. This script does all of that for you, gathering the info you need while you look at TikTok or whatever. All you have to do is feed it information and handle any CAPTCHAs it encounters.

## Contents
1. **[Installation](#installation)**

    1. [Download link](#download) (for advanced users)
    2. [Installation (Mac)](#mac)
    3. [Installation (PC)](#pc)
2. **[User's Guide](#usersguide)**
    1. [Database setup](#db)
    2. [Calendar Scraper](#cal)
    3. [Case Info Scraper](#case)
    4. [Updating Code](#update)
3. **[Troubleshooting](#troubleshooting)**


<a name="installation"></a>
# Installation

<a name="download"></a>
## Download Link

If you already have Python on your machine and you don't need help finding and installing packages out on the internet, [here's the GitLab repo link](https://gitlab.com/afriedman412/hmp_scraper).


<a name="mac"></a>
## Installation Instructions (Mac)
The majority of this is **running commands on Terminal.** If you don't know what that is, that's totally fine. It's already on your computer! Type command-space bar and type in 'Terminal' to load it up.

1. **Install [Homebrew](https://brew.sh/)**
Copy the code from the Homebrew page and run it on Terminal

    *Homebrew makes installing things easy and assures nothing you are installing will break anything else.*

2. **Install Python**
Run `brew install python`

    *Python is the programming language that we wrote all the code in.*

3. **Install Geckodriver**
Run `brew install geckodriver`

    *Geckodriver drives a web browser so you don't have to.*

4. **Install Firefox (if you don't already have it)**
Run `brew install --cask firefox`

    *If they don't remember when it was called Netscape, they are too young for you.*

5. **Download the HMP Scraper**
• Run `cd ~/Documents` to navigate to your Documents folder
• Run `git clone git@gitlab.com:afriedman412/hmp_scraper.git` to download the code to your computer

    **... If that doesn't work...**
    • [Download it from the GitLab repo](https://gitlab.com/afriedman412/hmp_scraper) by loading up that link, clicking on the download button (it's next to the blue "Clone" button) and choosing 'zip'
    • Move the downloaded .zip file to your Documents folder and unzip it

6. **Install Python packages**
• Run `cd ~/Documents/hmp_scraper-master` to navigate to the right folder
• Run `sh install.sh` to install all the extra Python code needed to run the Scraper

Remember where this folder is, because this is where the script lives!

### You are done!

<a name="pc"></a>
## Installation Instructions (PC)
I am a Mac person and I am copying and pasting instructions from the internet for all these steps, so if anything doesn't work please let me know!

1. **Install [Python](https://realpython.com/installing-python/#how-to-install-from-the-microsoft-store)**
I think the Microsoft Store version of Python should be fine for now, but let me know if it is not!

    *Python is the programming language that we wrote all the code in.*

2. **Download Geckodriver**
• [Download the latest version of Geckodriver here](https://github.com/mozilla/geckodriver/releases) -- use the link that   has "win-64" in it
• Unzip the .zip file

    *Geckodriver drives a web browser so you don't have to.*

3. **Install Geckodriver** (annoying but not difficult)
• Copy the path to the Geckodriver folder -- right-click on the folder, choose "Properties" and copy what's next to "Location:"
• Right-click on My Computer or This PC
• Select Properties
• Select "Advanced system settings"
• Click on "Environment Variables"
• Under "System Variables" select PATH
• Click "Edit", then click "New"
• Paste the path of Geckodriver folder (you it copied earlier)

4. **Install [Firefox](https://www.mozilla.org/en-US/firefox/new/) (if you don't have it)**

5. **Download the HMP Scraper**
• Run `cd ~\Documents` on the command line to navigate to your Documents folder
• Run `git clone git@gitlab.com:afriedman412/hmp_scraper.git` to download the code to your computer

    **... If that doesn't work...**
• [Download it from the GitLab repo](https://gitlab.com/afriedman412/hmp_scraper) by loading up that link, clicking on the download button (it's next to the blue "Clone" button) and choosing 'zip'
• Move the downloaded .zip file to your Documents folder and unzip it

6. **Install Python packages**
• Run `cd ~\Documents\hmp_scraper-master` to navigate to the right folder
• Run `pip3 install -r requirements.txt` to install all the extra Python code needed to run the Scraper

Remember where this folder is, because this is where the script lives!

### You are done!

<a name="db"></a>
## Database setup
###(You only need to do this if you are creating a scraper for a new county, and only once!)

First, sign up for an AWS account if you don't have one: [http://aws.amazon.com](http://aws.amazon.com)

### Create a database:
1. Go to [https://us-east-2.console.aws.amazon.com/rds/home](https://us-east-2.console.aws.amazon.com/rds/home)
2. Click “Create database”
3. Switch the “Engine type” to “MySQL”
4. Under “Templates” choose “Free tier”
5. Under “Settings”
    1. Set “DB instance identifier” to hmp-&lt;COUNTY>, replacing &lt;COUNTY> with the name of your county, e.g. hmp-monroe
    2. Click “Auto generate a password”
6. In “Additional configuration” enter hmp_&lt;COUNTY> for “Initial database name”
7. Click “Create database”
8. Click the name of your database
9. Wait a few minutes until the database is created, then click the “View connectivity details” button that appears on the top right.
11. Open up the `config.json` file in the "code" folder.
10. In any text editor, copy the following info from “Connection details” into config.json
    1. Endpoint —> db_host
    2. Master password —> db_password
    3. hmp_&lt;COUNTY> —> db_database
    4. Make sure the admin username and port match (they should by default)
11. Back on the main page,
    1. Click on the security group link all the way on the right (e.g. sg-575a4b20 - default) that was created for your database
    2. Click again on the Security group ID.
    3. On the security group detail page click the “Edit inbound rules” button.
    4. Click “Add rule”
    5. Change “Type” to MYSQL/Aurora
    6. From the “Source type” dropdown, choose “Anywhere”
    7. Click “Save rules”

<a name="usersguide"></a>
# User's Guide

<a name="cal"></a>
### Start
1. Open up terminal and run `cd ~/Documents/hmp_scraper-master` to navigate to where you saved the code.
* Run `flask run` to start the app.
* Open your web browswer of choice and load up [`http://127.0.0.1:5000/`](http://127.0.0.1:5000/) to open the app.

### Court Part
* Choose the court and court part you want to pull data from.
* If you don't see it in the provided options, go to [`http://127.0.0.1:5000/court_part_key`](http://127.0.0.1:5000/court_part_key), and enter the court_id and part_id into the form.

### Dates
* Format is important! YYYY-MM-DD only (ie 2021-05-07)
* All (?) the courts only keep data going back two weeks. If you choose a start date before that, it will either error out or automatically use the earliest start date it has. (I can't remember which.)

### Parse Only
* If the app crashes for whatever reason, you can choose this option to parse anything you already scraped and upload it to the database.

### Hit Submit!

The app should automatically launch Firefox and begin scraping. All you need to do is fill out the occasional CAPTCHA.

<a name="export"></a>
## Export Your Data
Your data lives in the database you set up on AWS, but you can download it any time you want by running the app and navigating to [`http://127.0.0.1:5000/export`](http://127.0.0.1:5000/export). Enter the start and end dates and, if you want, a file name, and press "Submit". It will save all the court appearances within the date range you specified.

<a name="update"></a>
## Updating Code
When we update the code, you can automatically update the local copy of the code. Here's how!

1. Navigate to the main folder (Mac - `cd ~/Documents/hmp_scraper-master`, PC - `cd ~\Documents\hmp_scraper-master`)
2. Run `git pull`


<a name="troubleshooting"></a>
# Troubleshooting
If either scraper seems like it stopped but Firefox isn't frozen, here's what you should do:

1. Check the other Firefox windows for a CAPTCHA. The script won't move until that's handled.
2. If there's no CAPTCHA, check the Terminal and make sure it's not just pausing to let things load.
3. If it doesn't say something like "waiting...", it will probably have thrown an error. If you want, take a screen shot and send it to me and I'll see if we can get things sorted.
